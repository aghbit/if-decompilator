__author__ = 'psarnicki'

import pickle
from os.path import exists
from os import makedirs

#PERSISTENCE
DOWNLOAD_ERROR_LOG_FILENAME = 'download_errors.txt'
DEFAULT_DB_FOLDER_NAME = 'movies_db'

#VIDEOS
VIDEO_RESOLUTION = "640x360"
VIDEO_FORMAT = "mp4"
VIDEO_MIN_DURATION_IN_SEC = 90
VIDEO_MAX_DURATION_IN_SEC = 1800


#Useful methods
#Note - will overwrite file
def save_as_pickle_file(filename, data):
    with open(filename, 'wb') as f:
        pickle.dump(data, f)


def load_pickle_file(filename):
    try:
        with open(filename, 'rb') as f:
            data = pickle.load(f)
            return data
    #Thrown when the file is initially empty
    except EOFError:
        return None


def log_downloading_error(info, file=DOWNLOAD_ERROR_LOG_FILENAME):
    with open(file, 'a') as f:
        print(info, file=f)


def create_dir_if_not_present(directory):
    if not exists(directory):
        makedirs(directory)


def create_file_if_not_present(path):
    try:
        with open(path, 'x') as f:
            pass
    except FileExistsError:
        pass