__author__ = 'Marcin && Bochen, psarnicki'

import urllib.request

import DownloadManagers
from bs4 import BeautifulSoup
from common import VIDEO_MAX_DURATION_IN_SEC, VIDEO_MIN_DURATION_IN_SEC
from db import video_db


def should_store_url(url, valid_tags, download_manager):

    if url is None:
        return False

    if not download_manager.is_link_valid(url):
        return False

    tags, duration = download_manager.get_tags_and_duration(url)

    if duration < VIDEO_MIN_DURATION_IN_SEC or duration > VIDEO_MAX_DURATION_IN_SEC:
        return False

    #TODO: Can extend this logic if needed
    for tag in tags:
        if tag.lower() in valid_tags:
            return True
    return False
"""
Args:
    @soup               - BeautifulSoup object representing page structure
    @valid_tags         - keywords to look for in video title
    @download_manager   - DownloadManager class object
"""
def get_associated_urls(soup, valid_tags, download_manager):
    associated_urls = []
    for link in soup.find_all('a'):
        associated_video_url = link.get('href')
        if should_store_url(associated_video_url, valid_tags, download_manager):
            associated_urls.append(associated_video_url)

    return associated_urls


"""
This method extracts links to associated videos and obtains metadata dict of the video specified.
Args:
    @url                    - path to the url without prefix as string
    @valid tags             - a set of tags as strings
    @download_manager       - DownloadManager class object
    @record_associated_urls - True/False value that indicates if associated urls should be stored

Returns:
    @(associated_urls, metadata)    - a single tuple
        associated_urls             - a list of of urls as strings
        metadata                    - dict (see DownloadManager class description)

Note: If the 'record_associated_url' parameter is set to false, 'associated_urls' will be an empty list.
"""
def extract(url, valid_tags, download_manager, record_associated_urls):
    page=urllib.request.urlopen(download_manager.base_url+url)
    soup = BeautifulSoup(page.read())
    associated_urls = []

    if record_associated_urls == True:
        associated_urls = get_associated_urls(soup, valid_tags, download_manager)
    video_metadata = download_manager.get_metadata_and_download(url)
    return associated_urls, video_metadata


"""
Starting at the specified urls, this method obtains a list of unique related urls with their metadata.
Args:
    @urls                   - a list of initial urls as strings
    @valid_tags             - a set of tags as strings
    @download_manager       - DownloadManager class object
    @storage_folder         - directory name that will hold downloaded movies
    @max_depth              - a number specifying the number of links to follow from the initial url
"""
def scraper(urls, valid_tags, download_manager_constructor, base_url, storage_folder_name, max_depth=3):

    database = video_db.VideoDB(storage_folder_name)
    download_manager = download_manager_constructor(database.db_path, base_url)
    urls_used = database.get_used_urls_set()

    level_ending_index = len(urls)
    current_level = 0
    #As we do not want to gather related urls at the last level of scraping.
    gather_urls = not (current_level == max_depth)

    #This is needed, as sometimes network / pafy exception is thrown
    try:
        for i, url in enumerate(urls):
            if url not in urls_used:
                urls_used.add(url)
                associated_urls, metadata_dict = extract(url, valid_tags, download_manager, gather_urls)

                database.add_video_dict(metadata_dict)

                if len(associated_urls) > 0:
                    urls.extend(associated_urls)

            if i == level_ending_index:
                current_level += 1
                level_ending_index = len(urls)
                gather_urls = not (current_level == max_depth)
    finally:
            print('No worries, data saved')
            database.save_db()



#Sample invocation
storage_folder = 'YTKoty'
base_url = 'https://www.youtube.com'
yt_download_manager = DownloadManagers.YTDownloadManager
initial_urls = ["/watch?v=2XV2bVamPCI"]
tags = set(['cat', 'cats']) #must be lowercase
max_depth = 1
scraper(initial_urls, tags, yt_download_manager, base_url, storage_folder, max_depth)