__author__ = 'psarnicki'

from common import VIDEO_RESOLUTION, VIDEO_FORMAT
import pafy
from os.path import join
from common import log_downloading_error

"""
This class is used to provide a coherent interface for videos located at different websites.
Note: Dict returned from 'get_metadata_and_download' must contain at least:
    -- 'file_path'
    -- 'url'
    entries. Also, this dict must NOT contain:
     -- 'id'
     entry, as this is used by video_db
"""


class DownloadManager(object):

    """
    @dest_dir - where to store the file
    @base_url - common part of links from given site. Eg. http://www.youtube.com'
    """
    def __init__(self, base_url):
        pass

    """
    Args:
        @url    - video url
    Returns:
        @(tags, duration) - a single tuple
        tags - list of strings
        duration - video duration in seconds
    """
    @staticmethod
    def get_tags_and_duration(url):
        pass

    """
    Args:
        @url    - video url
    Returns:
        metadata  - a single dict
        metadata must contain an least 'file_path'(relative path
            to the video) and 'url' entries.
    """
    def get_metadata_and_download(self, url):
        pass

    """
    Args:
        @url    - video url
    Returns:
        True/False - value indicating if the url represents a valid video
    """
    @staticmethod
    def is_link_valid(url):
        pass


class YTDownloadManager(DownloadManager):

    def __init__(self, dest_dir, base_url):
        self.dest_dir = dest_dir
        self.base_url = base_url

    @staticmethod
    def get_tags_and_duration(url):
        video = pafy.new(url)
        title = video.title
        keywords = title.split(' ')
        keywords.extend(video.keywords)
        return set(keywords), video.length

    def get_metadata_and_download(self, url):
        video = pafy.new(url)
        for stream in video.streams:
            if VIDEO_RESOLUTION == stream.resolution and VIDEO_FORMAT == stream.extension:

                file_path = join(self.dest_dir, video.videoid + '.' + stream.extension)
                stream.download(filepath=file_path)

                metadata = dict()
                metadata['file_path'] = video.videoid + '.' + stream.extension
                metadata['duration'] = video.length
                metadata['title'] = video.title
                metadata['rating'] = video.rating
                metadata['date_published'] = video.published
                metadata['view_count'] = video.viewcount
                metadata['url'] = url
                print('Downloaded: ' + str(url))
                return metadata

        info = 'Could not download YouTube video: ' + str(video.videoid) +\
               '.\nNo stream found with: %s resolution, %s format' % (VIDEO_RESOLUTION, VIDEO_FORMAT)
        log_downloading_error(info)

    @staticmethod
    def is_link_valid(url):
        if isinstance(url, str) and url.startswith("/watch") and url.find("&list") == -1:
            return True
        return False