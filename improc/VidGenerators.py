"""
All get_vid generators yield data in format of (frame_no,image)
All generators other than get_vid_vc_generator take a generator as an argument and iterate over it
to enable easy composition
"""

#Fuck error codes.


def get_vid_vc_generator(vc):
    """
    Basic generator, takes in a videocapture and yields numbered frames
    """
    frame_no = 0

    while True:
        success, image = vc.read()
        if success:
            yield frame_no, image
            frame_no += 1
        else:
            break


def get_vid_timelapse_generator(gen, lapse):
    it_no = 0
    for frame_no, image in gen:
        if it_no % lapse == 0:
            yield frame_no, image
        it_no += 1


def get_buffered_generator(gen, buffer_size):
    """
    A generic generator which first takes up to buffer_size elements from gen, and then yields them.
    Powerful for batch preprocessing.
    """
    buf = []
    for e in gen:
        buf.append(e)
        if len(buf) == buffer_size:
            for k in buf:
                yield k
            buf = []

        for k in buf:
            yield k

def get_frame_subset_generator(gen, frame_set):
    for no, f in gen:
        if no in frame_set:
            yield no, f

#Debugging only, this function is very very slow
def get_frame(vc, number):
    for no, f in get_vid_vc_generator(vc):
        if no == number:
            return f
    return None