#A terrible file for  quick and dirty testing. DO NOT USE IN ANY INTEGRATION.

import random
import numpy
import cv2

#Fuck error codes.
def get_vc_generator(vc):
    while True:
        success,image = vc.read()
        if success:
            yield image
        else:
            break


def get_derivative_generator(vc_gen):
    prev = next(vc_gen)
    for cur in vc_gen:
        yield cur - prev
        prev = cur

def get_sd_generator(vc):
    return get_derivative_generator(get_derivative_generator(get_vc_generator(vc)))

def get_colored_sd_generator(vc_gen):
    prev = next(vc_gen)
    cur = next(vc_gen)
    for nx in vc_gen:
        val = 2 * cur - prev - nx
        yield val
        cur,prev = nx,cur

def get_colored_generator(vc_gen):
    for cur in vc_gen:

        #val = (numpy.max(numpy.round(cur / 255),axis=2))
        #val = numpy.dstack((val, val, val)) * cur

        yield cur

def get_buffered_moving_median_generator(vc_gen, buffer_size):
    frame = next(vc_gen)
    fshape = frame.shape
    buffer = numpy.ndarray(shape=(buffer_size,fshape[0],fshape[1],fshape[2]))
    buffer[0] = frame
    curpos = 1

    good = False
    for frame in vc_gen:
        buffer[curpos] = frame
        curpos += 1
        if curpos == buffer_size:
            good = True
            curpos = 0
        if good:
            yield numpy.median(buffer,axis=0)

def get_piecewise_mean_generator(vc_gen,block):
    avg = next(vc_gen)
    sum = numpy.ndarray(shape=avg.shape,dtype=numpy.uint64)
    sum += avg
    count = 1.0

    for f in vc_gen:
        sum = sum + f
        count += 1.0
        if count > block:
            yield numpy.asarray(numpy.round(sum / count),dtype=numpy.uint8)
            count = 0
            sum.fill(0)

def get_maximizing_generator(vc_gen):
    for frame in vc_gen:
        numpy.apply_along_axis(max,2,frame)
        yield frame


def get_delta_skipsim_generator(vc_gen,thresh):
    prev = next(vc_gen)
    for f in vc_gen:
        tmp = f - prev
        if numpy.mean(numpy.square(tmp)) > thresh:
            yield f
            prev = f

def get_lapse_skipsim_generator(vc_gen,lapse):
    ctr = 0
    for f in vc_gen:
        if ctr % lapse == 0:
            yield f
        ctr += 1

def convolve(image,mask):
    return numpy.asarray(image * (mask / 255),dtype = numpy.uint8)


class DumbassMatching:
    def __init__(self, im1, im2):
        self.im1 = im1
        self.im2 = im2

    def average_frame(self,grd_gen):
        avg = next(grd_gen)
        sum = numpy.ndarray(shape=avg.shape,dtype=numpy.uint64)
        count = 1.0
        for f in grd_gen:
            sum = sum + f
            count += 1.0

            if round(count) % 1000 == 0:

                avg = numpy.asarray(numpy.round(255 * sum / numpy.max(sum)),dtype=numpy.uint8)
                cv2.imshow('Test',avg)
                cv2.waitKey(1)

        return numpy.asarray(numpy.round(255 * sum / numpy.max(sum)),dtype=numpy.uint8)

    def matching(self):
        vc = cv2.VideoCapture(self.im1)

        #Maybe?
        vc.set(3,640.0)
        vc.set(4,360.0)

        framecounter = 0
        cv2.namedWindow('Original')
        cv2.namedWindow('Test')

        vc2 = cv2.VideoCapture(self.im2)
        vc2.set(3,640.0)
        vc2.set(4,360.0)
        #Maybe?



        skipbuf = list(get_lapse_skipsim_generator(get_vc_generator(vc2), 30))
        skipfeat = [cv2.ORB().detectAndCompute(x[52:308,192:448], None) for x in skipbuf]


        for frame in get_lapse_skipsim_generator(get_vc_generator(vc), 30):
            framecounter += 1

            print(str(framecounter))

            #cv2.imshow('Original',frame)
            #cv2.waitKey(1)

            match,no,metric = self.find_best_match_for_frame(frame[52:308,192:448], skipbuf, skipfeat)
            #match[116:244,256:384] *= 0.5
            #frame[116:244,256:384] *= 0.5
            cv2.imshow('Test',match)
            cv2.imshow('Original',frame)
            print('Match with metric = ' + str(metric))
            if metric > 8:
                print('Frame %d matched with frame %d' % (framecounter * 30 - 30, no * 30 - 30))
                cv2.waitKey(1000)
            else:
                cv2.waitKey(1)

        #print(str())


        cv2.waitKey()
        cv2.destroyAllWindows()


    #The performance here is abysmal, but can be boosted so easily that's not a problem
    #- Reopen and reread per frame
    #- No caching
    #- Oblivious to local properties
    def find_best_match_for_frame(self, frame, skipbuf, skipfeat):
        bestcand = skipbuf[0]
        bestno = 0
        bestmetric = 0
        sift = cv2.ORB()
        kpf, desf = sift.detectAndCompute(frame,None)

        if desf == None:
            return (skipbuf[0],0,0)

        i = 0
        for cand,feat in zip(skipbuf,skipfeat):
            i += 1
            metric = 5
            kpc, desc = feat[0],feat[1]
            if desc == None:
                continue
            FLANN_INDEX_LSH = 6
            flann_params = dict(algorithm = FLANN_INDEX_LSH,
                   table_number = 6, # 12
                   key_size = 12, # 20
                   multi_probe_level = 1) #2

            flann = cv2.BFMatcher()
            matches = flann.knnMatch(desc,desf,k=2)

            good = []

            try:
                for m,n in matches:
                    if m.distance < 0.6*n.distance:
                        good.append([m])

                #img3 = cv2.drawMatchesKnn(frame,kpf,cand,kpc,matches[:10], flags=2)
                #cv2.imshow('Test',img3)
                #cv2.waitKey(100)
                metric = len(good)


                if metric > bestmetric:
                    bestcand, bestno, bestmetric = cand, i, metric
            except ValueError as e:
                pass


        return (bestcand,bestno,bestmetric)