import cv2

__author__ = "Krzysztof 'dickstra' Potempa"


class MatchingTestSuite:
    """
    Takes a vid keyframes generator, a db keyframes generator and a test list as named tuples:
    (vid_frame,matching_operator,matched_frame_params), allows for basic testing
    """

    def __init__(self, vid_generator, db_generator, tests, subject, match_test_type='coarse',
                 frame_tolerance=0, suite_complete=True):
        self.vid_generator = vid_generator
        self.db_generator = db_generator
        self.tests = tests
        self.subject = subject
        self.match_test_type = match_test_type
        self.frame_tolerance = frame_tolerance
        self.suite_complete = suite_complete

        if self.match_test_type == 'coarse':
            self.do_tests = self.do_tests_coarse
        else:
            raise ValueError('Unknown test type')  # One of the rare occasions where an exception is fine

    def condition_match(self, vf, mf, is_match, mv, tvf, matching_operator, tdbp):
        if matching_operator == 'on':
            return is_match and tdbp[0] < mf < tdbp[1]
        else:
            raise ValueError('Unknown matching operator')  # Should happen only on typos

    def do_tests_coarse(self):
        """
        Find general frame matchings without any densening on the local scale.
        This is meant to provide fast and reproducible testing function for experimental matching algos
        """

        sub_inst = self.subject(vid_generator=self.vid_generator, db_generator=self.db_generator)
        match_results = list(sub_inst.coarse_matching())
        test_results = []

        for vid_frame, matched_frame, is_match, match_value in match_results:
            matched_with_any_test_frame = False
            for test_vid_frame, matching_operator, test_db_param in self.tests:
                if test_vid_frame[0] < vid_frame < test_vid_frame[1]:
                    matched_with_any_test_frame = True
                    test_results.append((
                        self.condition_match(vid_frame, matched_frame, is_match, match_value,
                                             test_vid_frame, matching_operator, test_db_param),
                        vid_frame, is_match, match_value, matched_frame, test_db_param))

            if self.suite_complete and not matched_with_any_test_frame and is_match:
                test_results.append(('IncorrectExtraMatch', vid_frame, is_match, match_value, matched_frame, [0]))
        return test_results
