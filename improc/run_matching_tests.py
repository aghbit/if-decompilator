from functools import partial
from os import path
import time
import cv2
from improc import VidGenerators
from improc.MatchingTestSuite import MatchingTestSuite
from improc.matchers import RectKeypoint, Matcher


"""
    This uses a dynamic class loader for everything in the list, so as to keep the thing manageable
"""

#----- CONFIG START ------
#TODO: Maybe provide this as parameters?

test_dir = 'tests'
test_list = ['1_2', '1_3', '1_4', '2_3', '2_4', '3_4']
use_visuals = False


#    def __init__(self, vid_generator, db_generator, preprocess_fun,
#                 match_fun, match_threshold, match_predicate=operator.ge, inf=0):

def gen_matcher():
        return partial(Matcher.Matcher,
                       preprocess_fun=partial(RectKeypoint.rect_keypoint_preprocess,
                                              match_start_x=52,
                                              match_end_x=308,
                                              match_start_y=222,
                                              match_end_y=468),
                       match_fun=partial(RectKeypoint.rect_keypoint_match, ratio=0.6),
                       match_threshold=8)


#----- CONFIG END   ------


if __name__ == '__main__':
    for test_pack in test_list:
        print('Test pack %s:' % test_pack)
        module = __import__(test_dir + '.testpack_' + test_pack)  # A poor attempt to maintaing naming convention
        test_class = getattr(getattr(module, 'testpack_' + test_pack), 'TestPack')
        inst = test_class()

        if inst.get_test_magic() != "TESTPACK":
            raise ImportError('Testpack ' + test_pack + ' has bad magic')

        for (file1, file2, test_type, is_complete, list_of_tests) in inst.gen_tests():
            vc1 = cv2.VideoCapture(path.join(test_dir, file1))
            vc2 = cv2.VideoCapture(path.join(test_dir, file2))

            if not vc1.isOpened() or not vc2.isOpened():
                raise FileNotFoundError('Testpack ' + test_pack + ' has bad vid files')

            # Does this actually do anything? Test/enforce/...
            vc1.set(3, 640.0)
            vc1.set(4, 360.0)

            vc2.set(3, 640.0)
            vc2.set(4, 360.0)

            start_time = time.time()

            db_generator = VidGenerators.get_vid_timelapse_generator(VidGenerators.get_vid_vc_generator(vc2), 30)

            ts = MatchingTestSuite(VidGenerators.get_vid_timelapse_generator(VidGenerators.get_vid_vc_generator(vc1),
                                                                             30),
                                   db_generator,
                                   list_of_tests,
                                   gen_matcher(),
                                   suite_complete=is_complete)

            results = ts.do_tests()
            end_time = time.time()

            if use_visuals:
                cv2.namedWindow('Frame')
                cv2.namedWindow('MatchedWith')
                cv2.namedWindow('ShouldMatchByTest')

            print('\tMatching done in %s' % (end_time - start_time))
            print('\tFiles %s vs %s\n' % (file1, file2))

            for no, (success, target_frame, matched, match_value, matched_frame, should_match) in enumerate(results):
                print("\tTest %s - Success: %s, Frame: %s, IsMatch: %s, MV: %s, with frame %s, should match %s" %
                      (no, success, target_frame, matched, match_value, matched_frame, should_match))

                if use_visuals:
                    cv2.imshow('Frame', VidGenerators.get_frame(cv2.VideoCapture(path.join(test_dir, file1)),
                                                                target_frame))
                    cv2.imshow('MatchedWith', VidGenerators.get_frame(cv2.VideoCapture(path.join(test_dir, file2)),
                                                                      matched_frame))
                    cv2.imshow('ShouldMatchByTest', VidGenerators.get_frame(cv2.VideoCapture(path.join(test_dir, file2)),
                                                                            should_match[0]))
                    cv2.waitKey(1000)
