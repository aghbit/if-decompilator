import cv2
from improc import VidGenerators
from improc.matchers import RectKeypoint

__author__ = 'Kris'

if __name__ == '__main__':
    vid1_frameno = 0
    vid2_frameno = 0
    has_rect = False
    rect = (0, 0, 0, 0)
    v1n = ""
    v2n = ""
    v1f = None
    v2n = None

    cv2.namedWindow('Vid1')
    cv2.namedWindow('Vid2')

    while True:
        cmd = input('>')
        if cmd.startswith('open'):
            _, a, b = cmd.split()
            v1n, v2n = a, b
            v1f = cv2.VideoCapture(a)
            v2f = cv2.VideoCapture(b)
            print(v1f.isOpened() and v2f.isOpened())
        elif cmd.startswith('g'):
            _, a, b = cmd.split()
            a, b = int(a), int(b)
            vid1_frameno, vid2_frameno = a, b
            cv2.imshow('Vid1',VidGenerators.get_frame(cv2.VideoCapture(v1n), vid1_frameno))
            cv2.imshow('Vid2',VidGenerators.get_frame(cv2.VideoCapture(v2n), vid2_frameno))
            cv2.waitKey(1000)

        elif cmd.startswith('rect'):
            _, x1, x2, y1, y2 = cmd.split()
            x1, x2, y1, y2 = int(x1), int(x2), int(y1), int(y2)

            f1 = VidGenerators.get_frame(cv2.VideoCapture(v1n), vid1_frameno)
            f2 = VidGenerators.get_frame(cv2.VideoCapture(v2n), vid2_frameno)

            f1[x1:x2, y1:y2] *= 0.5
            f2[x1:x2, y1:y2] *= 0.5

            cv2.imshow('Vid1', f1)
            cv2.imshow('Vid2', f2)
            cv2.waitKey(1000)

        elif cmd.startswith('localbounds'):
            rkm = RectKeypoint.RectKeypointMatcher(match_start_x=52,
                                                          match_end_x=308,
                                                          match_start_y=222,
                                                          match_end_y=468,
                                                          match_coefficient=0.6,
                                                          match_threshold=8,
                                                          vid_generator=VidGenerators.get_vid_vc_generator(
                                                              cv2.VideoCapture(v1n)),
                                                          db_generator=None)
            res = rkm.local_matching()
            print(str(res))
