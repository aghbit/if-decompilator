"""
    This file implements an automated test generator using block <-> block matching
    by the RectKeypointMatcher, with L2^2 block distance. Its existence basically proves
    this project's most hard conceptual problem has been solved.
"""
import bisect
from collections import Counter
from functools import partial
import sys

import cv2
import numpy
from improc import VidGenerators
from improc.matchers import RectKeypoint
from improc.matchers.Matcher import Matcher


def l2_subframe_test(i1, i2, threshold):
    diff = i1[52:308, 222:468] - i2[52:308, 222:468]
    return numpy.mean(numpy.square(diff)) < threshold

def disjunctive_test(i1, i2):
    if l2_subframe_test(i1, i2, 60):
        return True

    k1 = RectKeypoint.rect_keypoint_preprocess(i1, match_start_x=52, match_end_x=308, match_start_y=222, match_end_y=468)
    k2 = RectKeypoint.rect_keypoint_preprocess(i2, match_start_x=52, match_end_x=308, match_start_y=222, match_end_y=468)
    return RectKeypoint.rect_keypoint_match(k1, k2, 0.6)


def local_partition(gen, lookback_count):
    rollbuffer = [0 for _ in range(lookback_count)]
    rollindex = 0
    retarray = []
    last_frame_no = 0
    can_test = False

    for frame_no, frame in gen:
        if not retarray:
            retarray.append(frame_no)

        if can_test:
            test_res = [disjunctive_test(frame, rb_frame[1]) for rb_frame in rollbuffer]
            if not any(test_res):
                retarray.append(frame_no)

        last_frame_no = frame_no
        rollbuffer[rollindex] = (frame_no, frame)
        rollindex += 1
        if rollindex >= lookback_count:
            if not can_test:
                can_test = True
            rollindex = 0

    retarray.append(last_frame_no+1)
    return retarray


def generate_test_lines(file1, file2, matching_periods_list):
    output = []
    output.append('"""')
    output.append('    THIS IS AN AUTOGENERATED TESTPACK')
    output.append('"""')
    output.append("")
    output.append("from os import path")
    output.append("")
    output.append("bin_dir = 'vids'")
    output.append("")
    output.append("")
    output.append("class TestPack:")
    output.append("    def get_test_magic(self):")
    output.append("        return 'TESTPACK'")
    output.append("")
    output.append("    def gen_tests(self):")
    output.append("        p = (")
    output.append("                path.join(bin_dir, '" + file1 + "'),")
    output.append("                path.join(bin_dir, '" + file2 + "'),")
    output.append("                'coarse',")
    output.append("                True,")
    output.append("                [")
    if matching_periods_list:
        for ((v1_start, v1_end), (v2_start, v2_end)) in matching_periods_list:
            output.append("                    ((" + str(v1_start) + ", " + str(v1_end) + "), 'on', (" + str(v2_start) + ", " + str(v2_end) + ")),")
        output[-1] = output[-1][:-1]
    output.append("                ]")
    output.append("            )")
    output.append("        yield p")

    return output


if __name__ == '__main__':
    vid1 = sys.argv[1]  # sysopy
    vid2 = sys.argv[2]

    vid1_partition = local_partition(VidGenerators.get_vid_vc_generator(cv2.VideoCapture(vid1)), 3)
    vid2_partition = local_partition(VidGenerators.get_vid_vc_generator(cv2.VideoCapture(vid2)), 3)

    #print('vid1 partition: %s' % str(vid1_partition))
    #print('vid2 partition: %s' % str(vid2_partition))

    rkm = Matcher(vid_generator=VidGenerators.get_vid_timelapse_generator(VidGenerators.get_vid_vc_generator(cv2.VideoCapture(vid1)), 30),
                  db_generator=VidGenerators.get_vid_timelapse_generator(VidGenerators.get_vid_vc_generator(cv2.VideoCapture(vid2)), 30),
                  preprocess_fun=partial(RectKeypoint.rect_keypoint_preprocess,
                                         match_start_x=52,
                                         match_end_x=308,
                                         match_start_y=222,
                                         match_end_y=468),
                  match_fun=partial(RectKeypoint.rect_keypoint_match, ratio=0.6),
                  match_threshold=8)

    matching = rkm.coarse_matching()

    votes = Counter()

    for vid_frame, db_frame, is_match, match_value in matching:
        if not is_match:
            continue

        v_pos = bisect.bisect_left(vid1_partition, vid_frame)
        d_pos = bisect.bisect_left(vid2_partition, db_frame)

        #print('Vote by %s and %s to match (%s,%s) to (%s,%s) with power %s' % (vid_frame, db_frame, vid1_partition[v_pos-1],
        #                                                          vid1_partition[v_pos],
        #                                                          vid2_partition[d_pos-1],
        #                                                          vid2_partition[d_pos],
        #                                                         match_value))

        votes[((vid1_partition[v_pos-1], vid1_partition[v_pos]), (vid2_partition[d_pos-1], vid2_partition[d_pos]))] += 1

    match_list = []

    for match_bounds, vote_count in votes.items():
        if vote_count < 2:
            continue # Hehe prog wyborczy

        match_list.append(match_bounds)

    for line in generate_test_lines(vid1, vid2, match_list):
        print(line)