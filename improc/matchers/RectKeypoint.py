"""
This file provides a workable implementation of a keypoint matching functions
"""
__author__ = "Krzysztof 'dickstra' Potempa"

import cv2


def rect_keypoint_preprocess(image, match_start_x, match_end_x, match_start_y, match_end_y):
    return cv2.ORB().detectAndCompute(
            image[match_start_x:match_end_x, match_start_y:match_end_y], None)


def rect_keypoint_match(kp1, kp2, ratio):
    desc1, desc2 = kp1[1], kp2[1]

    if desc1 is None or len(desc1) < 2:
        return 0
    if desc2 is None or len(desc2) < 2:
        return 0

    bf_matcher = cv2.BFMatcher(cv2.NORM_HAMMING)
    matches = bf_matcher.knnMatch(desc1, desc2, k=2)

    good = 0
    for m, n in matches:
        if m.distance < ratio*n.distance:
            good += 1

    return good
