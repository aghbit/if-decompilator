from collections import defaultdict
import operator

__author__ = "Krzysztof 'dickstra' Potempa"

"""
This file provides a generic matcher, details to be plugged in by preprocess and match
"""


class Matcher:
    def __init__(self, vid_generator, db_generator, preprocess_fun,
                 match_fun, match_threshold, match_predicate=operator.ge, inf=0):
        self.vid_generator = vid_generator
        self.db_generator = db_generator
        self.preprocess = preprocess_fun
        self.match = match_fun
        self.match_threshold = match_threshold
        self.best_matches = defaultdict(lambda: (0, inf))
        self.match_predicate = match_predicate
        self.preprocessed = [(k[0], self.preprocess(k[1])) for k in vid_generator]

    def coarse_matching(self):
        for candidate_frame_no, candidate_frame in self.db_generator:
            cand_pre = self.preprocess(candidate_frame)

            for vid_frame_no, vid_pre in self.preprocessed:
                match_value = self.match(cand_pre, vid_pre)

                if self.match_predicate(match_value, self.best_matches[vid_frame_no][1]):
                    self.best_matches[vid_frame_no] = candidate_frame_no, match_value

        for vid_frame_no, (best_candidate, best_metric) in sorted(self.best_matches.items(), key=lambda x: x[0]):
            yield (vid_frame_no, best_candidate, self.match_predicate(best_metric, self.match_threshold), best_metric)

