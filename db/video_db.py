__author__ = 'psarnicki'

from common import DEFAULT_DB_FOLDER_NAME
from common import load_pickle_file, save_as_pickle_file
from common import create_dir_if_not_present, create_file_if_not_present
from os.path import join, dirname
from collections import defaultdict


class VideoDB(object):

    def __init__(self, db_folder_name=DEFAULT_DB_FOLDER_NAME):
        self.config_file = join(dirname(__file__), 'db_config.pickle')
        self.video_id = self.get_video_id()

        #Path to downloaded videos directory
        self.db_path = join(dirname(__file__), db_folder_name)
        create_dir_if_not_present(self.db_path)

        #Path to db file
        self.db_filepath = join(self.db_path, db_folder_name + '.pickle')
        create_file_if_not_present(self.db_filepath)
        self.db = self.load_db()

    """
    Adds new video metadata dict to the database. Takes care of id's and dict of subvideos.
    Args:
        @metadata_dict   - value to insert
    """
    def add_video_dict(self, metadata_dict):
        metadata_dict['id'] = self.video_id
        self.video_id += 1
        metadata_dict['subvideos'] = defaultdict(set)
        self.db.append(metadata_dict)

    """
    Args:
        @video_id   - id as integer
    Returns:
        @video_dict - when found
        None        - otherwise
    Note: Search performance can be improved if needed
    """
    def get_video_dict(self, video_id):
        for video_dict in self.db:
            if video_dict['id'] == video_id:
                return video_dict

        return None

    """
    Removes video_dict and all its occurrences as a subvideo.
    Even if video_dict is not found, all its occurrences as a subvideo are still removed.
    Args:
        @video_id   - id as integer
    """
    def remove_video_dict(self, video_id):
        index_to_remove = None
        for index, video_dict in enumerate(self.db):
            if video_id in video_dict['subvideos']:
                del video_dict['subvideos'][video_id]

            if video_dict['id'] == video_id:
                index_to_remove = index

        if index_to_remove is not None:
            del self.db[index_to_remove]

    """
    Adds a new entry to the subvideo dict of a given video dict.
        Args:
            @enclosing_video_dict   - dict of the video in which the subvideo was found
            @subvideo_id            - id of the video as integer
            @subvideo_data          - (some, tuple,)
    Note: This method does not perform any checking, so make sure arguments make sense.
    """
    def add_subvideo_tuple(self, enclosing_video_dict, subvideo_id, subvideo_data):
        enclosing_video_dict['subvideos'][subvideo_id].add(subvideo_data)

    def videos_gen(self):
        return iter(self.db)

    def get_used_urls_set(self):
        return set(some_dict.get('url') for some_dict in self.db)

### Not part of the interface ###
    def save_db(self):
        save_as_pickle_file(self.db_filepath, self.db)
        save_as_pickle_file(self.config_file, self.video_id)

    """
    Returns a list of dicts of already stored videos, or an empty list
    """
    def load_db(self):
        db = load_pickle_file(self.db_filepath)
        if db is not None:
            return db
        return []

    """
    Used to persist value of video_id
    """
    def get_video_id(self):
        data = None
        try:
            data = load_pickle_file(self.config_file)
        except FileNotFoundError:
            create_file_if_not_present(self.config_file)

        if data is not None:
            return data

        #initial video id
        return 0